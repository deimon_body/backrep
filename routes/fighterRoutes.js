const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid ,deleteFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/',function(req,res){
    res.send(FighterService.getAllFighters());
})

router.get('/:id',function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const getCurrentFighter = FighterService.search(id);
    res.send(getCurrentFighter);
})

router.put('/:id',updateFighterValid ,function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const newFighterUpdate = FighterService.updateFighter(id,req.body);
    res.send(newFighterUpdate);
})
router.post("/",createFighterValid,function(req,res){
    const dataFighter = FighterService.createNewFighter(req.body);
    if(!dataFighter.health){
        dataFighter.health = 100;
    }
    res.send(dataFighter);
})

router.delete('/:id',deleteFighterValid,function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const deletedFighter = FighterService.deleteFighter(id);
    res.send(deletedFighter)
})
module.exports = router;