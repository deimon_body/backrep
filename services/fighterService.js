const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    createNewFighter(data){
        const fighter = FighterRepository.create(data);
        if(!fighter) return null     
        return fighter;
    }
    idCoincidence(id){
        const data = FighterRepository.getAll();
        const findById = data.find((fighter)=>fighter.id == id); 
        if(findById) return true;
        return false;
    }
    findCoincidence(currentFighter){
        const data = FighterRepository.getAll();
        const findByName = data.find((fighter)=>fighter.name == currentFighter.name); 
        
        if(findByName) return true; //find coinciedence
        return false; //not found coincidence
    }
    getAllFighters(){
        return FighterRepository.getAll();
    }

    search(id){
        const item = FighterRepository.getOne(id)
        if(!item){
            res.status(404).send("Fighter was not found");
            return null;
        }
        return item;
    }

    updateFighter(id,data){
        const item = FighterRepository.update(id,data);
        if(!item) {
            res.status(404).send("Fighter was not found");
            return null;
        }
        return item;
    }
    deleteFighter(id){
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();