const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    saveNewUser(user){
        const newUser = UserRepository.create(user);
        return newUser;
    }
    idCoincidence(id){
        const data = UserRepository.getAll();
        const findById = data.find((user)=>user.id == id); 
        if(findById) return true;
        return false;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            
            return null;
        }
        return item;
    }
    findCoincidence(currentUser){
        const data = UserRepository.getAll();
        const findByEmail = data.find((user)=>user.email == currentUser.email); 
        const findByTelephone = data.find((user)=>user.phoneNumber == currentUser.phoneNumber);
        if(findByEmail || findByTelephone) return true; 
        return false; 
    }

    updateUser(id,data){
        const item = UserRepository.update(id,data);
        if(!item){
            return null
        } 
        return item;
    }

    deleteUser(id){
        const item = UserRepository.delete(id);
        return item
    }
    
    getAllUsers(){
        return UserRepository.getAll();
    }
}

module.exports = new UserService();